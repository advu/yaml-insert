#!/usr/bin/python3

import argparse
import sys
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', help="Import filename")
parser.add_argument('--output', '-o', help="Output filename")
parser.add_argument('--data', '-d', help="File with import param")
parser.add_argument('--name', '-n', help="Param name")
args = parser.parse_args()

with open(args.data, "r") as import_file:
    import_data = import_file.read()

import_node_exist = False
for node in yaml.compose(import_data).value:
    if node[0].value == args.name:
        import_node = node
        import_node_exist = True
        break

if not import_node_exist:
    print(f"Name '{args.name}' not found in file '{args.data}'")
    sys.exit(1)

with open(args.input, "r") as input_file:
    input_data = input_file.read()

tree = yaml.compose(input_data)
exist = False
for node in tree.value:
    if node[0].value == args.name:
        found_node = node
        exist = True

if exist:
    found_index = tree.value.index(found_node)
    tree.value.pop(found_index)
    tree.value.insert(found_index, import_node)
else:
    tree.value.append(import_node)

with open(args.output, 'w') as output_file:
    output_file.write(yaml.serialize(tree))

