#!/usr/bin/python3

import argparse
import sys
from ruamel.yaml import YAML

parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', help="Import filename")
parser.add_argument('--output', '-o', help="Output filename")
parser.add_argument('--data', '-d', help="File with import param")
args = parser.parse_args()

yaml = YAML()

with open(args.data, "r") as import_file:
    import_data = yaml.load(import_file.read())

with open(args.input, "r") as input_file:
    input_data = yaml.load(input_file.read())

for key, value in import_data.items():
    input_data[key] = value

with open(args.output, 'w') as output_file:
    yaml.dump(input_data, output_file)

