# yaml-insert

## main2.py

1. Leaves comments.
2. Insert all data from file

requirements:

```bash
pip install ruamel.yaml
```

usage:

```bash
$ cat input.yaml
one: one
# Here comment
two: two
three: three

$ cat data.yaml
one: 1
two:
- two
- 2

$ ./main2.go -i input.yaml -d data.yaml -o output.yaml

$ cat output.yaml
one: 1
# Here comment
two:
- two
- 2
three: three

$ diff input.yaml output.yaml
1c1
< one: one
---
> one: 1
3c3,5
< two: two
---
> two:
> - two
> - 2
```

## main.py

1. Removes comments
2. Insert only one key

```bash
$ cat input.yaml
one: 1
two: 2
three: 3

$ cat data.yaml
two:
- 2
- two

$ ./main.py -i input.yaml \
            -o output.yaml \
            -d data.yaml \
            -n two

$ cat output.yaml
one: 1
two:
- 2
- two
three: 3
```

